# DeezLoader Reborn V3.0.0
DeezLoader Reborn is here to replace the old DeezLoader V2.3.1.<br/>
With this software you can download high-qualiy music and enjoy.

**Warning: Linux users are required to install 'flac' package in order to download FLAC files(For Tagging)**<br/>
**Warning: In order to download FLAC quality you need to turn on HIFI in settings.**<br/>
**Please post problems in issues, do not PM me or comment in the reddit post**

# Features
- Download FLAC/MP3-320 music content from Deezer(FLAC needs to be turned on in the settings 'turn on HIFI')
- Search musics
- Use Deezer links as an alternative for Searching and downloading inside the software
- Does not requires an account at all
- Download multiple musics at a time
- Download Albums and artists
- Tagging system on MP3-320 and FLAC!!!!!!(Linux users have to install 'flac' package[apt-get,pacman...])
- Simple and user friendly

# How to run
- Download the installer and run it.
- Run DeezLoader.

# How to compile
- Clone the repository
- Run compile.sh/bat or run the command 'npm install && npm run dist'
- Go to dist and you will see the installer and the software folder
- Have fun(Easy Peasy)

# Download Links
- Windows x64: [link](https://mega.nz/#!tY1X3SrB!imuns0jujBF9I3sfSOfIogpTIkRHwy9iiB7fUy-W-kc)
- Windows x86: [link](https://mega.nz/#!9Ms2wIBT!hAtv8xSxoQyhlUgloRE9jrhcvt_bY4Fi9Gghxhp0UTw)
- Linux x64: [link](https://mega.nz/#!IQdRUIgY!onaVMmkGHhkA0dbnebMYgPzyVQ9NGSJtoWh56OzPKj8)
- Linux x86: [link](https://mega.nz/#!5UMUkSLZ!NfZNnSTxAg1Vd06TajshzeOo13tCvMWZZ83mLhvwK2U)

# Virus total
- Windows x64: [link](https://www.virustotal.com/#/file/45c585f3c350b52c96eca3d6a29bbd019aa0050d1bdfa74b2e35a08ee2d7013d)
- Windows x86: [link](https://www.virustotal.com/#/file/e6353d0352a19a3370f32a5cc02f9fcfb5e7a0922363c55343c7b5881e707d0f)
- Linux x64: [link](https://www.virustotal.com/#/file/d93c05f148ab05623855259b18d85cf11f980bc85b159ccb72a70be98596add7)
- Linux x86: [link](https://www.virustotal.com/#/file/54c52fef2f033e28bd89c5cd8ba1eea3d6ad10e2895b2c7f6396f916ed545b6e)

# Credits
## Original Developer
[ZzMTV](https://boerse.to/members/zzmtv.3378614/)

## Former Maintainers
[ParadoxalManiak](https://github.com/ParadoxalManiak)<br/>
[snwflake](https://github.com/snwflake)

## Maintainers
[ExtendLord](https://github.com/ExtendLord)

# Disclaimer
- I am not responsible for the usage of this program by other people.
- I do not recommend you doing this illegally or against Deezer's terms of service.
- This project is licensed under [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)
